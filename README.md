# Habitat Navigation
## Usage Instructions
Use the shell script to install Habitat (lab, and sim).

Prerequisite:
- Linux-based system (preferable ubuntu 18/20/21), they all should work.
- Git command.
- Conda (Anaconda or Miniconda).
- Source command.
- Additional commands that requires sudo privileges at the beginning of the script.

Installation and run Instructions:
- $ chmod +x habitat_installation.sh
- $ source habitat_installation.sh
- $ conda activate habitat_env
- $ python viewer.py

Navigation instructions:
- Detailed when the simulation begins and also below.

Key Commands:
-------------
    esc:        Exit the application.
    'h':        Display this help message.
    'm':        Cycle mouse interaction modes (disable)
    
    Navigation Commands (important):
    'c':        Start the experiment.
    'i':        Issue a new instruction from the current agent's position to the goal.
    'p':        Set the next random seed and reinitialize the environment.
    
    
    Agent Controls:
    'wsad':     Move the agent's body forward/backward and left/right.
    'zx':       Move the agent's body up/down.
    arrow keys: Turn the agent's body left/right and camera look up/down.
    

## V1 related modifications
- [x] Changed the dataset to [Replica](https://github.com/facebookresearch/Replica-Dataset)
- [x] Modified object selection to take only objects that are not "None" and have a specific category. To work on the new dataset and simulate real-world scenarios where some objects are not well recognized.
- [x] Added more left/right direction angles and messages.
- [x] Added more messages for other instructions.
- [x] Added the semantic information in the image to select the reference object with the distance of that object from the goal as a scoring function. It has already been fine-tuned to give good results. However, there is still the possibility for improvement.
- [x] Added extremely simple fallback instructions if no landmarks are found.
- [x] Selected four test environments that no-code development has been done on them.

  - 1 Room 
  - 2 Apartments
  - 1 Office
  
- [x] Selected several random initializations (start point & goal) for these test environments 
- [x] Changed the goal generation method to take into consideration more than one step ahead.
- [x] Fixed some bugs in the code

  - Related to the angle. 
  - Figures generation.
  - Dataset related ( there was an issue with the navmesh generation code when it comes to the new dataset, so it has been changed now).
  - Other technical errors.
  
- [x] Added a simple GUI to display and test the environment.
- [x] Refactor, document, test & deployed.
- [x] Script has not been changed the required dataset will be installed from git request.
- [ ] Ideas that were not added:
  - [ ] Give more specification to objects (size, distance from the agent, ...) it's needed because if there are two objects of the same category within view, then it might be a bit confusing. 
