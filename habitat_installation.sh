# Required commands to build habitat-sim from source
sudo apt-get update || true
sudo apt-get install -y --no-install-recommends \
     libjpeg-dev libglm-dev libgl1-mesa-glx libegl1-mesa-dev mesa-utils xorg-dev freeglut3-dev


# Create directory to store the repos and data
mkdir habitat_env
cd habitat_env

# Create conda environment with a particular python version and possible a cmake version
conda create -n habitat_env -y python=3.7 #cmake=3.14.0
conda activate habitat_env


# Clone Habitat-sim & Habitat-lab repos
git clone https://github.com/facebookresearch/habitat-lab.git
git clone https://github.com/facebookresearch/habitat-sim.git

# Install Habitat-lab core and baselines
cd habitat-lab
set +e
pip install -r ./requirements.txt
reqs=(./habitat_baselines/**/requirements.txt)
pip install "${reqs[@]/#/-r}"
set -e
python setup.py develop --all
pip install . #Reinstall to trigger sys.path update
pip install -e .

# Install Habitat-sim from source
cd ..
cd habitat-sim
pip install -r requirements.txt
python setup.py install --bullet

# Install additional python packages
conda install jupyterlab -y
