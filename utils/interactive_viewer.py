import ctypes
import math
import os
import sys
import time
import heapq
import random
import operator
from enum import Enum
from typing import Any, Callable, Dict, List, Optional, Tuple
from scipy.spatial import distance
from PIL import Image
from scipy.interpolate import interp1d
from collections import Counter

flags = sys.getdlopenflags()
sys.setdlopenflags(flags | ctypes.RTLD_GLOBAL)

import magnum as mn
from magnum.platform.glfw import Application

import habitat_sim
from utils.settings import default_sim_settings, make_cfg
from habitat_sim import physics
from habitat_sim.logging import LoggingContext, logger
from habitat.utils.visualizations import maps
from habitat_sim.utils import common as utils
from habitat_sim.utils.common import d3_40_colors_rgb

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.patches as patches

from utils.mouse_actions import MouseMode, MouseGrabber
from utils.timer import Timer
import threading


class HabitatSimInteractiveViewer(Application):
    def __init__(self, sim_settings: Dict[str, Any], env_name, step_label, instruction_label) -> None:

        # Set the environment name, title, and essential configurations
        self.env_name = env_name
        self.instruction_label = instruction_label
        self.step_label = step_label

        # environment configuration
        configuration = self.Configuration()

        # adjust OpenGl display size
        configuration.size = mn.Vector2i(1600, 900)
        configuration.title = f"Experimental Environment {self.env_name}"
        Application.__init__(self, configuration)

        self.sim_settings: Dict[str:Any] = sim_settings
        self.fps: float = 30.0
        self.debug_bullet_draw = False

        # cache most recently loaded URDF file for quick-reload
        self.cached_urdf = ""
        self.angle_state = 1

        # enable required sensors
        self.rgb_sensor = True
        self.depth_sensor = True
        self.semantic_sensor = True

        # define starting parameters
        self.start = None
        self.goal = None
        self.next_step = None
        self.angle = None
        self.step_counter = 0.0

        # define random seeds for new random goals and intialization
        if self.env_name == "apartment_0":
            self.random_seeds = [343, 75, 325, 8, 42, 45]
        elif self.env_name == "room_0":
            # self.random_seeds = [0, 11, 38, 43, 56, 75, 84]
            self.random_seeds = [11, 38, 43, 56, 84]
        elif self.env_name == "frl_apartment_0":
            self.random_seeds = [0, 38, 62, 85, 99, 123, 378]
        elif self.env_name == "office_0":
            self.random_seeds = [6, 11, 25, 35, 43, 56, 85, 99, 434]
        else:
            self.random_seeds = [48, 0, 5, 9, 75, 11, 43, 3988, ]

        self.seed_index = 0

        # set proper viewport size
        self.viewport_size: mn.Vector2i = mn.gl.default_framebuffer.viewport.size()
        self.sim_settings["width"] = self.viewport_size[0]
        self.sim_settings["height"] = self.viewport_size[1]
        self.sim_settings["sensor_height"] = 1.4
        self.sim_settings["radius"] = 0.05
        self.sim_settings["color_sensor"] = self.rgb_sensor
        self.sim_settings["depth_sensor"] = self.depth_sensor
        self.sim_settings["semantic_sensor"] = self.semantic_sensor

        self.agent_position = []
        self.agent_checkpoints = []
        self.angles_checkpoints = []
        self.angles = []

        self.meters_per_pixel = 0.025
        self.distance_threshold = .5

        self.NEAR_DIST = 0.5  # max distance to "near" points 
        self.STEP_LENGTH = 0.45  # length of a human step
        self.BAD_CATEGORIES = ['wall', 'misc', 'ceiling', 'floor', 'void',
                               'objects', 'appliances', 'lighting', 'pillar', '', ' ', 'blinds',
                               "switch", "wall-plug", "plant-stand", "comforter", "shower-stall", "plant-stand",
                               "major-appliance", "faucet", "base-cabinet", "switch", "handrail", "cooktop",
                               "panel", "wall-plug", "plant-stand", "comforter", "shower-stall", "plant-stand",
                               "faucet", "base-cabinet", "panel", "utensil-holder", "small-appliance", "pipe", "rug", "book", "lamp", "basket", "rack"]
        self.bad_size_categories = ["door"]

        self.important_objects = ['door', 'table', 'desk', 'fireplace',
                                  'refrigerator', 'tv_monitor', 'tv-screen',
                                  'plant', 'indoor-plant', 'counter', 'picture',
                                  'sofa', 'bathtab', 'bed', 'sink', 'stairs', 'stair']

        self.obj_category_landmark_score = {
            "chair": 10, "toilet": 10, "desk": 10,
            "bike": 10, "refrigerator": 10,

            "tv_monitor": 9, "stool": 9, "tv-screen": 9, "door": 4,

            "table": 8, "fireplace": 8, "cabinet": 8,
            "plant": 4, "counter": 5, "book": 4,
            "pillow": 4, "beanbag": 4,

            "picture": 7, "sink": 4, "box": 4,
            "chest_of_drawers": 4,

            "bench": 6, "clock": 6,

            "sofa": 6, "bathtub": 5, "bed": 7,
            "shower": 5, "shoe": 5, "shelf": 5,
            "handbag": 5,

            "gym_equipment": 4, "window": 2, "vent": 4,
            "plate": 4, "chopping-board": 4, "pot": 4,
            "rack": 4, "basket": 4, "nightstand": 4,
            "vase": 4, "bottle": 4, "stair": 4,
            "wall-cabinet": 4, "bowl": 4, "pan": 4,
            "camera": 4,

            "towel": 3, "cushion": 3, "clothes": 3,
            "blinds": 3, "seating": 3,
            "candle": 3, "blanket": 3, "mat": 3,
            "countertop": 3, "cup": 3,
            "knife-block": 3, "tissue-paper": 3, "clothing": 3,
            "scarf": 3,
            "lamp": 3, "book": 3,
            "switch": 2, "wall-plug": 2, "plant-stand": 2,
            "comforter": 2, "shower-stall": 2, "plant-stand": 2,
            "major-appliance": 2, "faucet": 2, "base-cabinet": 2,
            "switch": 2, "handrail": 2, "cooktop": 2,
            "panel": 2, "wall-plug": 2, "plant-stand": 2,
            "comforter": 2, "shower-stall": 2, "plant-stand": 2,
            "faucet": 2, "base-cabinet": 2, "panel": 2,
            "utensil-holder": 2, "small-appliance": 2, "pipe": 2,
            "rug": 2
        }

        # set up our movement map
        key = Application.KeyEvent.Key
        self.pressed = {
            key.UP: False,
            key.DOWN: False,
            key.LEFT: False,
            key.RIGHT: False,
            key.A: False,
            key.D: False,
            key.S: False,
            key.W: False,
            key.X: False,
            key.Z: False,
        }

        # set up our movement key bindings map
        key = Application.KeyEvent.Key
        self.key_to_action = {
            key.UP: "look_up",
            key.DOWN: "look_down",
            key.LEFT: "turn_left",
            key.RIGHT: "turn_right",
            key.A: "move_left",
            key.D: "move_right",
            key.S: "move_backward",
            key.W: "move_forward",
            key.X: "move_down",
            key.Z: "move_up",
        }

        # cycle mouse utilities
        self.mouse_interaction = MouseMode.LOOK
        self.mouse_grabber: Optional[MouseGrabber] = None
        self.previous_mouse_point = None

        # toggle physics simulation on/off
        self.simulating = True

        # toggle a single simulation step at the next opportunity if not
        # simulating continuously.
        self.simulate_single_step = False

        # configure our simulator
        self.cfg: Optional[habitat_sim.simulator.Configuration] = None
        self.sim: Optional[habitat_sim.simulator.Simulator] = None
        self.reconfigure_sim()

        # extract the semantic scene (if any)
        self.scene = self.sim.semantic_scene

        # map ids to objects
        self.obj_id_to_obj = {int(obj.id.split("_")[-1]): obj for obj in self.scene.objects
                              if obj is not None if obj.category is not None}
        # save high important objects' volumes
        self.obj_id_to_volume = {int(obj.id.split("_")[-1]): np.prod(obj.aabb.sizes)
                                 for obj in self.obj_id_to_obj.values()
                                 if obj.category is not None}
        # if obj.category.name() in self.important_objects}

        # assign a region to each object
        self.obj_id_to_region = self.assign_region_to_objects()
        self.time_since_last_simulation = 0.0

        self.previous_pick = None
        LoggingContext.reinitialize_from_env()
        logger.setLevel("INFO")

        self.print_help_text()

    def retrieve_next_subgoal(self, current_position,
                              min_distance_threshold=1.,
                              max_distance_threshold=10.):
        """
        Retrive the next stop point in the navigation processed, based on the distance from the agent's current
        position and a pre-defined set of important landmarks/objects
        """

        if len(self.obj_id_to_volume.items()) < 1:
            return None

        possible_targets = []
        for obj_id, obj_volume in self.obj_id_to_volume.items():
            obj = self.obj_id_to_obj[obj_id]

            if obj.category.name() in self.important_objects:
                 if obj.category.name() != self.previous_pick:
                        if max_distance_threshold > distance.euclidean(obj.aabb.center, current_position) > min_distance_threshold:
                            possible_targets.append(obj_id)

        if len(possible_targets) > 0:
            obj_id = random.choice(possible_targets)
            obj = self.obj_id_to_obj[obj_id]
            self.obj_id_to_volume.pop(obj_id)
            self.previous_pick = obj.category.name()
        else:
            obj = None

        return obj

    def assign_region_to_objects(self, distance_threshold=1., count=10):
        """
        Estimate which region does an object belongs to 
        giving it's label and nearest nighbors lables based on predefined list of objects.
        """

        kitchen_objs = ["refrigerator", "sink",
                        "stove", "oven", "microwave", "table"]

        bathroom_objs = ["toilet", "sink",
                         "bathtub", "shower", "towel"]

        livingroom_objs = ["table", "fireplace",
                           "tv-screen", "tv_monitor", "sofa"]

        bedroom_objs = ["bed"]

        obj_id_to_region = {}
        assigned_region = None
        for obj_id, obj in self.obj_id_to_obj.items():
            kitchen_counter, bathroom_counter, livingroom_counter, bedroom_counter = 0, 0, 0, 0
            for _, close_obj in self.obj_id_to_obj.items():
                dist = distance.euclidean(obj.aabb.center, close_obj.aabb.center)
                print("dist" , dist)
                print(close_obj.category.name())
                if abs(obj.aabb.center[1] - close_obj.aabb.center[1]) < 2.:
                    if dist < distance_threshold:
                        if close_obj.category.name() in kitchen_objs:
                            kitchen_counter += 1
                        elif close_obj.category.name() in bathroom_objs:
                            bathroom_counter += 1
                        elif close_obj.category.name() in livingroom_objs:
                            livingroom_counter += 1
                        elif close_obj.category.name() in bedroom_objs:
                            bedroom_counter += 1

            if obj.category.name() in kitchen_objs:
                kitchen_counter += 1.2
            elif obj.category.name() in bathroom_objs:
                bathroom_counter += 1.2
            elif obj.category.name() in livingroom_objs:
                livingroom_counter += 1.2
            elif obj.category.name() in bedroom_objs:
                bedroom_counter += 1.2

            tmp_list = np.array([bedroom_counter * 3, bathroom_counter, kitchen_counter, livingroom_counter])
            max_value = np.max(tmp_list)

            if max_value >= 3.2:
                max_index = np.argmax(tmp_list)

                if max_index == 0:
                    assigned_region = "bedroom"
                elif max_index == 1:
                    assigned_region = "bathroom"
                elif max_index == 2:
                    assigned_region = "kitchen"
                else:  # max_index == 3:
                    assigned_region = "livingroom"

            # if assigned_region is not None:
            obj_id_to_region[obj_id] = assigned_region

        return obj_id_to_region

    def debug_draw(self):
        """
        Additional draw commands to be called during draw_event.
        """
        if self.debug_bullet_draw:
            render_cam = self.render_camera.render_camera
            proj_mat = render_cam.projection_matrix.__matmul__(render_cam.camera_matrix)
            self.sim.debug_draw(proj_mat)

    def draw_event(
            self,
            simulation_call: Optional[Callable] = None,
            global_call: Optional[Callable] = None,
            active_agent_id_and_sensor_name: Tuple[int, str] = (0, "color_sensor"),
    ) -> None:
        """
        Calls continuously to re-render frames and swap the two frame buffers
        at a fixed rate.
        """
        agent_acts_per_sec = self.fps

        mn.gl.default_framebuffer.clear(
            mn.gl.FramebufferClear.COLOR | mn.gl.FramebufferClear.DEPTH
        )

        # Agent actions should occur at a fixed rate per second
        self.time_since_last_simulation += Timer.prev_frame_duration
        num_agent_actions: int = self.time_since_last_simulation * agent_acts_per_sec
        self.move_and_look(int(num_agent_actions))

        # Occasionally a frame will pass quicker than 1/60 seconds
        if self.time_since_last_simulation >= 1.0 / self.fps:
            if self.simulating or self.simulate_single_step:
                # step physics at a fixed rate
                # In the interest of frame rate, only a single step is taken,
                # even if time_since_last_simulation is quite large
                self.sim.step_world(1.0 / self.fps)
                self.simulate_single_step = False
                if simulation_call is not None:
                    simulation_call()
            if global_call is not None:
                global_call()

            # reset time_since_last_simulation, accounting for potential overflow
            self.time_since_last_simulation = math.fmod(
                self.time_since_last_simulation, 1.0 / self.fps
            )

        keys = active_agent_id_and_sensor_name

        self.sim._Simulator__sensors[keys[0]][keys[1]].draw_observation()
        agent = self.sim.get_agent(keys[0])
        self.render_camera = agent.scene_node.node_sensor_suite.get(keys[1])
        self.debug_draw()
        self.render_camera.render_target.blit_rgba_to_default()
        mn.gl.default_framebuffer.bind()

        self.swap_buffers()
        Timer.next_frame()
        self.redraw()

    def default_agent_config(self) -> habitat_sim.agent.AgentConfiguration:
        """
        Set up our own agent and agent controls
        """

        make_action_spec = habitat_sim.agent.ActionSpec
        make_actuation_spec = habitat_sim.agent.ActuationSpec
        MOVE, LOOK = 0.07, 1.5

        # all of our possible actions' names
        action_list = [
            "move_left",
            "turn_left",
            "move_right",
            "turn_right",
            "move_backward",
            "look_up",
            "move_forward",
            "look_down",
            "move_down",
            "move_up",
        ]

        action_space: Dict[str, habitat_sim.agent.ActionSpec] = {}

        # build our action space map
        for action in action_list:
            actuation_spec_amt = MOVE if "move" in action else LOOK
            action_spec = make_action_spec(
                action, make_actuation_spec(actuation_spec_amt)
            )
            action_space[action] = action_spec

        # add the required sensors specifications
        sensor_specs = []

        color_sensor_spec = habitat_sim.CameraSensorSpec()
        color_sensor_spec.uuid = "color_sensor"
        color_sensor_spec.sensor_type = habitat_sim.SensorType.COLOR
        color_sensor_spec.resolution = [self.sim_settings["height"], self.sim_settings["width"]]
        color_sensor_spec.position = [0.0, self.sim_settings["sensor_height"], 0.0]
        color_sensor_spec.sensor_subtype = habitat_sim.SensorSubType.PINHOLE
        sensor_specs.append(color_sensor_spec)

        depth_sensor_spec = habitat_sim.CameraSensorSpec()
        depth_sensor_spec.uuid = "depth_sensor"
        depth_sensor_spec.sensor_type = habitat_sim.SensorType.DEPTH
        depth_sensor_spec.resolution = [self.sim_settings["height"], self.sim_settings["width"]]
        depth_sensor_spec.position = [0.0, self.sim_settings["sensor_height"], 0.0]
        depth_sensor_spec.sensor_subtype = habitat_sim.SensorSubType.PINHOLE
        sensor_specs.append(depth_sensor_spec)

        semantic_sensor_spec = habitat_sim.CameraSensorSpec()
        semantic_sensor_spec.uuid = "semantic_sensor"
        semantic_sensor_spec.sensor_type = habitat_sim.SensorType.SEMANTIC
        semantic_sensor_spec.resolution = [self.sim_settings["height"], self.sim_settings["width"]]
        semantic_sensor_spec.position = [0.0, self.sim_settings["sensor_height"], 0.0]
        semantic_sensor_spec.sensor_subtype = habitat_sim.SensorSubType.PINHOLE

        sensor_specs.append(semantic_sensor_spec)

        self.cfg.agents[self.agent_id].sensor_specifications = sensor_specs
        agent_config = habitat_sim.agent.AgentConfiguration(
            height=self.sim_settings["sensor_height"],
            radius=self.sim_settings["radius"],
            sensor_specifications=sensor_specs,
            action_space=action_space,
            body_type="cylinder",
        )
        return agent_config

    def reconfigure_sim(self) -> None:
        """
        Utilizes the current `self.sim_settings` to configure and set up a new
        `habitat_sim.Simulator`, and then either starts a simulation instance, or replaces
        the current simulator instance, reloading the most recently loaded scene
        """

        # configure our sim_settings but then set the agent to our default
        self.cfg = make_cfg(self.sim_settings)
        self.agent_id: int = self.sim_settings["default_agent"]
        self.cfg.agents[self.agent_id] = self.default_agent_config()

        if self.sim is None:
            self.sim = habitat_sim.Simulator(self.cfg)

        else:  # edge case
            if self.sim.config.sim_cfg.scene_id == self.cfg.sim_cfg.scene_id:
                # we need to force a reset, so change the internal config scene name
                self.sim.config.sim_cfg.scene_id = "NONE"
            self.sim.reconfigure(self.cfg)
        # post reconfigure
        self.active_scene_graph = self.sim.get_active_scene_graph()
        self.default_agent = self.sim.get_agent(self.agent_id)
        self.agent_body_node = self.default_agent.scene_node
        self.render_camera = self.agent_body_node.node_sensor_suite.get("color_sensor")
        # set sim_settings scene name as actual loaded scene
        self.sim_settings["scene"] = self.sim.curr_scene_name

        # compute NavMesh if not already loaded by the scene.
        if not self.sim.pathfinder.is_loaded:
            self.navmesh_config_and_recompute()
        self.sim.pathfinder.seed(self.random_seeds[self.seed_index])
        self.start_point = self.sim.pathfinder.get_random_navigable_point()

        agent_state = habitat_sim.AgentState()
        agent_state.position = self.start_point
        self.sim.agents[self.agent_id].set_state(agent_state)

        Timer.start()
        self.step = -1

        cam = self.render_camera
        cam.zoom(0.4)

    def move_and_look(self, repetitions: int) -> None:
        """
        This method is called continuously with `self.draw_event` to monitor
        any changes in the movement keys map `Dict[KeyEvent.key, Bool]`.
        When a key in the map is set to `True` the corresponding action is taken.
        """
        # avoids unecessary updates to grabber's object position
        if repetitions == 0:
            return

        key = Application.KeyEvent.Key
        agent = self.sim.agents[self.agent_id]
        press: Dict[key.key, bool] = self.pressed
        act: Dict[key.key, str] = self.key_to_action

        action_queue: List[str] = [act[k] for k, v in press.items() if v]

        for _ in range(int(repetitions)):
            [agent.act(x) for x in action_queue]

        # update the grabber transform when our agent is moved
        if self.mouse_grabber is not None:
            # update location of grabbed object
            self.update_grab_position(self.previous_mouse_point)

        # Save agent movement
        if len(self.agent_position) > 1:
            if np.any(self.agent_position[-1] != agent.state.position):
                self.agent_position.append(agent.state.position)

                if self.start is not None:
                    self.step_counter += distance.euclidean(self.agent_position[-1],
                                                            self.agent_position[-2]) / self.STEP_LENGTH
                    self.step_label.set(f"Steps: {round(self.step_counter, 1)}")
        else:
            self.agent_position.append(agent.state.position)

        if len(self.angles) > 1:
            if self.angles[-1] != agent.get_state().rotation.angle():
                self.angles.append(agent.get_state().rotation.angle())

            if self.angles[-1] - self.angles[-2] < 0:
                self.angle_state = -1
            else:
                self.angle_state = 1
        else:
            self.angles.append(agent.get_state().rotation.angle())
            # max(round(d/self.STEP_LENGTH), 1)

    def invert_gravity(self) -> None:
        """
        Sets the gravity vector to the negative of it's previous value. This is
        a good method for testing simulation functionality.
        """
        gravity: mn.Vector3 = self.sim.get_gravity() * -1
        self.sim.set_gravity(gravity)

    def key_press_event(self, event: Application.KeyEvent) -> None:
        """
        Handles `Application.KeyEvent` on a key press by performing the corresponding functions.
        If the key pressed is part of the movement keys map `Dict[KeyEvent.key, Bool]`, then the
        key will be set to False for the next `self.move_and_look()` to update the current actions.
        """

        key = event.key
        pressed = Application.KeyEvent.Key
        mod = Application.InputEvent.Modifier

        shift_pressed = bool(event.modifiers & mod.SHIFT)
        alt_pressed = bool(event.modifiers & mod.ALT)

        # TODO: remove extra actions

        if key == pressed.ESC:
            event.accepted = True
            self.exit_event(Application.ExitEvent)
            return

        elif key == pressed.H:
            self.print_help_text()

        elif key == pressed.TAB:
            # NOTE: (+ALT) - reconfigure without cycling scenes
            if not alt_pressed:
                # cycle the active scene from the set available in MetadataMediator
                inc = -1 if shift_pressed else 1
                scene_ids = self.sim.metadata_mediator.get_scene_handles()
                cur_scene_index = 0
                if self.sim_settings["scene"] not in scene_ids:
                    matching_scenes = [
                        (ix, x)
                        for ix, x in enumerate(scene_ids)
                        if self.sim_settings["scene"] in x
                    ]
                    if not matching_scenes:
                        logger.warning(
                            f"The current scene, '{self.sim_settings['scene']}', is not in the list, starting cycle at index 0."
                        )
                    else:
                        cur_scene_index = matching_scenes[0][0]
                else:
                    cur_scene_index = scene_ids.index(self.sim_settings["scene"])

                next_scene_index = min(
                    max(cur_scene_index + inc, 0), len(scene_ids) - 1
                )
                self.sim_settings["scene"] = scene_ids[next_scene_index]
            self.reconfigure_sim()
            logger.info(
                f"Reconfigured simulator for scene: {self.sim_settings['scene']}"
            )

        elif key == pressed.SPACE:
            if not self.sim.config.sim_cfg.enable_physics:
                logger.warn("Warning: physics was not enabled during setup")
            else:
                self.simulating = not self.simulating
                logger.info(f"Command: physics simulating set to {self.simulating}")

        elif key == pressed.PERIOD:
            if self.simulating:
                logger.warn("Warning: physic simulation already running")
            else:
                self.simulate_single_step = True
                logger.info("Command: physics step taken")

        elif key == pressed.COMMA:
            self.debug_bullet_draw = not self.debug_bullet_draw
            logger.info(f"Command: toggle Bullet debug draw: {self.debug_bullet_draw}")

        elif key == pressed.T:
            # load URDF
            fixed_base = alt_pressed
            urdf_file_path = ""
            if shift_pressed and self.cached_urdf:
                urdf_file_path = self.cached_urdf
            else:
                urdf_file_path = input("Load URDF: provide a URDF filepath:").strip()

            if not urdf_file_path:
                logger.warn("Load URDF: no input provided. Aborting.")
            elif not urdf_file_path.endswith((".URDF", ".urdf")):
                logger.warn("Load URDF: input is not a URDF. Aborting.")

            elif os.path.exists(urdf_file_path):
                self.cached_urdf = urdf_file_path
                aom = self.sim.get_articulated_object_manager()
                ao = aom.add_articulated_object_from_urdf(
                    urdf_file_path, fixed_base, 1.0, 1.0, True
                )
                ao.translation = self.agent_body_node.transformation.transform_point(
                    [0.0, 1.0, -1.5]
                )
            else:
                logger.warn("Load URDF: input file not found. Aborting.")

        elif key == pressed.M:
            self.cycle_mouse_mode()
            logger.info(f"Command: mouse mode set to {self.mouse_interaction}")

        elif key == pressed.V:
            self.invert_gravity()
            logger.info("Command: gravity inverted")

        elif key == pressed.N:
            if shift_pressed:
                logger.info(f"Command: recompute navmesh")
                self.navmesh_config_and_recompute()
            else:
                if self.sim.pathfinder.is_loaded:
                    self.sim.navmesh_visualization = not self.sim.navmesh_visualization
                    logger.info("Command: toggle navmesh")
                else:
                    logger.warn("Warning: recompute navmesh first")

        elif key == pressed.C:
            self.set_random_goal()
            print(self.start)
            print(self.goal[-1])
            self.navmesh_visualization()

        elif key == pressed.B:
            # self.draw_agent_navmesh()
            # self.assign_region_to_objects()

            pass
        elif key == pressed.O:
            self.print_scene_recur(self.scene)

        # start new test run
        elif key == pressed.P:
            self.start, self.goal, self.angle = None, None, None
            self.initialize_new_location()
            self.set_random_goal()
            self.navmesh_visualization()
            self.instruction_label.set("")

        # retrieve new instruction from the current position to the goal
        elif key == pressed.G:
            if len(self.obj_id_to_volume) < 1:
                logger.info("Done!")
                self.instruction_label.set(
                    "There are no more targets, feel free to check the environment as you please!")

            # if self.goal == None:
            self.set_next_subgoal()
            self.navmesh_visualization()

            self.angle = self.sim.agents[self.agent_id].get_state().rotation.angle()

            self.start, self.next_step = self.generate_random_step(self.goal)
            try:
                if len(self.next_step) > 2:
                    tmp = self.check_points_distance(self.start + self.next_step)
                    self.start = tmp[0]
                    self.next_step = tmp[1]
                else:
                    self.next_step = self.next_step[0]
            except:
                self.next_step = self.next_step[0]

            self.agent_checkpoints.append(self.start)
            self.angles_checkpoints.append(math.degrees(self.angle))

            self.draw_agent_navmesh(self.goal[-1], self.agent_checkpoints, self.angles_checkpoints)
            _, tmp_y, _ = self.euler_from_quaternion(self.sim.agents[self.agent_id].get_state().rotation.x,
                                                     self.sim.agents[self.agent_id].get_state().rotation.y,
                                                     self.sim.agents[self.agent_id].get_state().rotation.z,
                                                     self.sim.agents[self.agent_id].get_state().rotation.w)

            if tmp_y < 0:
                if self.angle < math.pi:
                    self.angle = (math.pi * 2) - self.angle

            instruction = self.instruction(self.start, self.angle, self.next_step)
            logger.info(f"New Instruction: {instruction}")
            logger.info(f"Distance from the goal: {distance.euclidean(self.goal[-1], self.start)}")
            self.instruction_label.set(instruction)

            # if distance.euclidean(self.goal[-1], self.start) < self.distance_threshold:
            #     self.draw_agent_navmesh(self.goal[-1], self.agent_checkpoints, self.angles_checkpoints)
            #     logger.info("Goal should have been reached by now!!!!!\nResetting")
            #     self.instruction_label.set("Goal is reached!!!!!\nResetting to a new point")
            #     self.start, self.goal, self.angle = None, None, None
            #     self.agent_checkpoints = []
            #     self.agent_position = []
            #     self.initialize_new_location()
            #
            #     self.set_random_goal()
            #     self.navmesh_visualization()
            #     self.step_label.set(f"Steps: 0.0")

        elif key == pressed.I:

            if len(self.obj_id_to_volume) < 1:
                logger.info("Done!")
                self.instruction_label.set(
                    "There are no more targets, feel free to check the environment as you please!")

            self.step_counter = 0.0
            if self.goal == None:
                self.set_random_goal()
                self.navmesh_visualization()

            self.angle = self.sim.agents[self.agent_id].get_state().rotation.angle()

            self.start, self.next_step = self.generate_random_step(self.goal)

            if len(self.next_step) > 2:
                tmp = self.check_points_distance(self.start + self.next_step)
                self.start = tmp[0]
                self.next_step = tmp[1]
            else:
                self.next_step = self.next_step[0]

            self.agent_checkpoints.append(self.start)
            self.angles_checkpoints.append(math.degrees(self.angle))

            self.draw_agent_navmesh(self.goal[-1], self.agent_checkpoints, self.angles_checkpoints)
            _, tmp_y, _ = self.euler_from_quaternion(self.sim.agents[self.agent_id].get_state().rotation.x,
                                                     self.sim.agents[self.agent_id].get_state().rotation.y,
                                                     self.sim.agents[self.agent_id].get_state().rotation.z,
                                                     self.sim.agents[self.agent_id].get_state().rotation.w)

            if tmp_y < 0:
                if self.angle < math.pi:
                    self.angle = (math.pi * 2) - self.angle

            instruction = self.instruction(self.start, self.angle, self.next_step)
            logger.info(f"New Instruction: {instruction}")
            logger.info(f"Distance from the goal: {distance.euclidean(self.goal[-1], self.start)}")
            self.instruction_label.set(instruction)

            if distance.euclidean(self.goal[-1], self.start) < self.distance_threshold:
                self.draw_agent_navmesh(self.goal[-1], self.agent_checkpoints, self.angles_checkpoints)
                logger.info("Goal should have been reached by now!!!!!\nResetting")
                self.instruction_label.set("Goal is reached!!!!!\nResetting to a new point")
                self.start, self.goal, self.angle = None, None, None
                self.agent_checkpoints = []
                self.agent_position = []
                self.initialize_new_location()

                self.set_random_goal()
                self.navmesh_visualization()
                self.step_label.set(f"Steps: 0.0")

        # update map of moving/looking keys which are currently pressed
        if key in self.pressed:
            self.pressed[key] = True
        event.accepted = True
        self.redraw()

    def set_next_subgoal(self):
        if self.goal is None:
            found_path = False
            current_position = self.sim.agents[self.agent_id].get_state().position
            while not found_path:
                subgoal = self.retrieve_next_subgoal(current_position)
                found_path, self.start, self.goal = self.generate_steps_towards_subgoal(subgoal.aabb.center)
            logger.info("Command: Initailizing a new goal")
        else:
            logger.info("Command: Generate new instructions to reach the goal")

    def generate_steps_towards_subgoal(self, subgoal_position):
        path = habitat_sim.ShortestPath()
        found_path = False
        # while not found_path:
        start = self.sim.agents[self.agent_id].get_state().position
        path.requested_start = start
        print(subgoal_position)
        path.requested_end = subgoal_position
        found_path = self.sim.pathfinder.find_path(path)

        return found_path, path.points[0], path.points[-2:]

    def set_random_goal(self):
        if self.goal is None:
            self.start, self.goal = self.generate_random_goal()
            logger.info("Command: Initailizing a new goal")
        else:
            logger.info("Command: Generate new instructions to reach the goal")

        self.agent_checkpoints.append(self.start)
        self.angles_checkpoints.append(math.degrees(self.sim.agents[self.agent_id].get_state().rotation.angle()) % 360)

    def initialize_new_location(self) -> None:
        """
        Change the pre-defined random seed of the environment to create a 
        new goal and a new starting position
        """
        self.seed_index += 1
        if self.seed_index > len(self.random_seeds) - 1:
            logger.warn("Warning: all test are now complete for this environment!")
        else:
            try:
                self.reconfigure_sim()
                logger.info(
                    f"Reconfigured simulator for scene: {self.sim_settings['scene']}, environment name: {self.env_name}")
            except TypeError:
                logger.error(
                    f"Reconfiguration error using seed: {self.seed_index}, increasing the index and reconfiguring...")
                self.initialize_new_location()

    def key_release_event(self, event: Application.KeyEvent) -> None:
        """
        Handles `Application.KeyEvent` on a key release. When a key is released, if it
        is part of the movement keys map `Dict[KeyEvent.key, Bool]`, then the key will
        be set to False for the next `self.move_and_look()` to update the current actions.
        """
        key = event.key

        # update map of moving/looking keys which are currently pressed
        if key in self.pressed:
            self.pressed[key] = False
        event.accepted = True
        self.redraw()

    def mouse_move_event(self, event: Application.MouseMoveEvent) -> None:
        """
        Handles `Application.MouseMoveEvent`. When in LOOK mode, enables the left
        mouse button to steer the agent's facing direction. When in GRAB mode,
        continues to update the grabber's object positiion with our agents position.
        """
        button = Application.MouseMoveEvent.Buttons
        # if interactive mode -> LOOK MODE
        if event.buttons == button.LEFT and self.mouse_interaction == MouseMode.LOOK:
            # if self.mouse_interaction == MouseMode.LOOK:
            agent = self.sim.agents[self.agent_id]
            delta = self.get_mouse_position(event.relative_position) / 2
            action = habitat_sim.agent.ObjectControls()
            act_spec = habitat_sim.agent.ActuationSpec

            # left/right on agent scene node
            action(agent.scene_node, "turn_right", act_spec(delta.x))

            # up/down on cameras' scene nodes
            action = habitat_sim.agent.ObjectControls()
            sensors = list(self.agent_body_node.subtree_sensors.values())
            [action(s.object, "look_down", act_spec(delta.y), False) for s in sensors]

        # if interactive mode is TRUE -> GRAB MODE
        elif self.mouse_interaction == MouseMode.GRAB and self.mouse_grabber:
            # update location of grabbed object
            self.update_grab_position(self.get_mouse_position(event.position))

        self.previous_mouse_point = self.get_mouse_position(event.position)
        self.redraw()
        event.accepted = True

    def mouse_press_event(self, event: Application.MouseEvent) -> None:
        """
        Handles `Application.MouseEvent`. When in GRAB mode, click on
        objects to drag their position. (right-click for fixed constraints)
        """
        button = Application.MouseEvent.Button
        physics_enabled = self.sim.get_physics_simulation_library()

        # if interactive mode is True -> GRAB MODE
        if self.mouse_interaction == MouseMode.GRAB and physics_enabled:
            render_camera = self.render_camera.render_camera
            ray = render_camera.unproject(self.get_mouse_position(event.position))
            raycast_results = self.sim.cast_ray(ray=ray)

            if raycast_results.has_hits():
                hit_object, ao_link = -1, -1
                hit_info = raycast_results.hits[0]

                if hit_info.object_id >= 0:
                    # we hit an non-staged collision object
                    ro_mngr = self.sim.get_rigid_object_manager()
                    ao_mngr = self.sim.get_articulated_object_manager()
                    ao = ao_mngr.get_object_by_id(hit_info.object_id)
                    ro = ro_mngr.get_object_by_id(hit_info.object_id)

                    if ro:
                        # if grabbed an object
                        hit_object = hit_info.object_id
                        object_pivot = ro.transformation.inverted().transform_point(
                            hit_info.point
                        )
                        object_frame = ro.rotation.inverted()
                    elif ao:
                        # if grabbed the base link
                        hit_object = hit_info.object_id
                        object_pivot = ao.transformation.inverted().transform_point(
                            hit_info.point
                        )
                        object_frame = ao.rotation.inverted()
                    else:
                        for ao_handle in ao_mngr.get_objects_by_handle_substring():
                            ao = ao_mngr.get_object_by_handle(ao_handle)
                            link_to_obj_ids = ao.link_object_ids

                            if hit_info.object_id in link_to_obj_ids:
                                # if we got a link
                                ao_link = link_to_obj_ids[hit_info.object_id]
                                object_pivot = (
                                    ao.get_link_scene_node(ao_link)
                                        .transformation.inverted()
                                        .transform_point(hit_info.point)
                                )
                                object_frame = ao.get_link_scene_node(
                                    ao_link
                                ).rotation.inverted()
                                hit_object = ao.object_id
                                break
                    # done checking for AO

                    if hit_object >= 0:
                        node = self.agent_body_node
                        constraint_settings = physics.RigidConstraintSettings()

                        constraint_settings.object_id_a = hit_object
                        constraint_settings.link_id_a = ao_link
                        constraint_settings.pivot_a = object_pivot
                        constraint_settings.frame_a = (
                                object_frame.to_matrix() @ node.rotation.to_matrix()
                        )
                        constraint_settings.frame_b = node.rotation.to_matrix()
                        constraint_settings.pivot_b = hit_info.point

                        # by default use a point 2 point constraint
                        if event.button == button.RIGHT:
                            constraint_settings.constraint_type = (
                                physics.RigidConstraintType.Fixed
                            )

                        grip_depth = (
                                hit_info.point - render_camera.node.absolute_translation
                        ).length()

                        self.mouse_grabber = MouseGrabber(
                            constraint_settings,
                            grip_depth,
                            self.sim,
                        )
                    else:
                        logger.warn("Oops, couldn't find the hit object. That's odd.")
                # end if didn't hit the scene
            # end has raycast hit
        # end has physics enabled

        self.previous_mouse_point = self.get_mouse_position(event.position)
        self.redraw()
        event.accepted = True

    def mouse_scroll_event(self, event: Application.MouseScrollEvent) -> None:
        """
        Handles `Application.MouseScrollEvent`. When in LOOK mode, enables camera
        zooming (fine-grained zoom using shift) When in GRAB mode, adjusts the depth
        of the grabber's object. (larger depth change rate using shift)
        """
        scroll_mod_val = (
            event.offset.y
            if abs(event.offset.y) > abs(event.offset.x)
            else event.offset.x
        )
        if not scroll_mod_val:
            return

        # use shift to scale action response
        shift_pressed = bool(event.modifiers & Application.InputEvent.Modifier.SHIFT)
        alt_pressed = bool(event.modifiers & Application.InputEvent.Modifier.ALT)
        ctrl_pressed = bool(event.modifiers & Application.InputEvent.Modifier.CTRL)

        # if interactive mode is False -> LOOK MODE
        if self.mouse_interaction == MouseMode.LOOK:
            # use shift for fine-grained zooming
            mod_val = 1.01 if shift_pressed else 1.1
            mod = mod_val if scroll_mod_val > 0 else 1.0 / mod_val
            cam = self.render_camera
            cam.zoom(mod)
            self.redraw()

        elif self.mouse_interaction == MouseMode.GRAB and self.mouse_grabber:
            # adjust the depth
            mod_val = 0.1 if shift_pressed else 0.01
            scroll_delta = scroll_mod_val * mod_val
            if alt_pressed or ctrl_pressed:
                # rotate the object's local constraint frame
                agent_t = self.agent_body_node.transformation_matrix()
                # ALT - yaw
                rotation_axis = agent_t.transform_vector(mn.Vector3(0, 1, 0))
                if alt_pressed and ctrl_pressed:
                    # ALT+CTRL - roll
                    rotation_axis = agent_t.transform_vector(mn.Vector3(0, 0, -1))
                elif ctrl_pressed:
                    # CTRL - pitch
                    rotation_axis = agent_t.transform_vector(mn.Vector3(1, 0, 0))
                self.mouse_grabber.rotate_local_frame_by_global_angle_axis(
                    rotation_axis, mn.Rad(scroll_delta)
                )
            else:
                # update location of grabbed object
                self.mouse_grabber.grip_depth += scroll_delta
                self.update_grab_position(self.get_mouse_position(event.position))
        self.redraw()
        event.accepted = True

    def mouse_release_event(self, event: Application.MouseEvent) -> None:
        """
        Release any existing constraints.
        """
        del self.mouse_grabber
        self.mouse_grabber = None
        event.accepted = True

    def update_grab_position(self, point: mn.Vector2i) -> None:
        """
        Accepts a point derived from a mouse click event and updates the
        transform of the mouse grabber.
        """
        # check mouse grabber
        if not self.mouse_grabber:
            return

        render_camera = self.render_camera.render_camera
        ray = render_camera.unproject(point)

        rotation: mn.Matrix3x3 = self.agent_body_node.rotation.to_matrix()
        translation: mn.Vector3 = (
                render_camera.node.absolute_translation
                + ray.direction * self.mouse_grabber.grip_depth
        )
        self.mouse_grabber.update_transform(mn.Matrix4.from_(rotation, translation))

    def get_mouse_position(self, mouse_event_position: mn.Vector2i) -> mn.Vector2i:
        """
        This function will get a screen-space mouse position appropriately
        scaled based on framebuffer size and window size.  Generally these would be
        the same value, but on certain HiDPI displays (Retina displays) they may be
        different.
        """
        scaling = mn.Vector2i(self.framebuffer_size) / mn.Vector2i(self.window_size)
        return mouse_event_position * scaling

    def cycle_mouse_mode(self) -> None:
        """
        This method defines how to cycle through the mouse mode.
        """
        if self.mouse_interaction == MouseMode.LOOK:
            self.mouse_interaction = MouseMode.GRAB
        elif self.mouse_interaction == MouseMode.GRAB:
            self.mouse_interaction = MouseMode.LOOK

    def display_save_map(self, topdown_map, map_path, key_points=None, display=True) -> None:

        plt.figure(figsize=(12, 8))
        ax = plt.subplot(1, 1, 1)
        ax.axis("off")

        if display:
            plt.imshow(topdown_map)

        if key_points is not None:
            for point in key_points:
                plt.plot(point[0], point[1], marker="o", markersize=10, alpha=0.8)
        plt.savefig(map_path)

    def display_save_sample(self, rgb_obs, image_path, display=True) -> None:

        plt.figure(figsize=(12, 8))
        ax = plt.subplot(1, 1, 1)
        ax.axis("off")

        rgb_img = Image.fromarray(rgb_obs, mode="RGBA")

        if display:
            plt.imshow(rgb_img)
        plt.savefig("test1.jpg")

    def display_sample(self, rgb_obs, semantic_obs=np.array([]), depth_obs=np.array([])) -> None:
        """
        Display the retrieved images with the ground truth depth, and semantic information
        """

        rgb_img = Image.fromarray(rgb_obs, mode="RGBA")

        arr = [rgb_img]
        titles = ["rgb"]
        if semantic_obs.size != 0:
            semantic_img = Image.new("P", (semantic_obs.shape[1], semantic_obs.shape[0]))
            semantic_img.putpalette(d3_40_colors_rgb.flatten())
            semantic_img.putdata((semantic_obs.flatten() % 40).astype(np.uint8))
            semantic_img = semantic_img.convert("RGBA")
            arr.append(semantic_img)
            titles.append("semantic")

        if depth_obs.size != 0:
            depth_img = Image.fromarray((depth_obs / 10 * 255).astype(np.uint8), mode="L")
            arr.append(depth_img)
            titles.append("depth")

        plt.figure(figsize=(20, 16))
        for i, data in enumerate(arr):
            ax = plt.subplot(1, 3, i + 1)
            ax.axis("off")
            ax.set_title(titles[i])
            plt.imshow(data)
        plt.show(block=False)

    def get_topdown_view(self):
        """
        Return an image containg the topdown view of the environment
        """
        lower_bound, _ = self.sim.pathfinder.get_bounds()
        height = lower_bound[1]

        top_down_map = maps.get_topdown_map(
            self.sim.pathfinder, height, meters_per_pixel=self.meters_per_pixel
        )
        recolor_map = np.array(
            [[255, 255, 255], [128, 128, 128], [0, 0, 0]], dtype=np.uint8
        )
        # top_down_map = maps.colorize_topdown_map(top_down_map,  fog_of_war_desat_amount=0.0)
        top_down_map = recolor_map[top_down_map]

        return top_down_map

    def draw_agent_navmesh(self, goal=None, checkpoints=None, angles=None) -> None:
        """
        Draw each position the agent went through on the topdown view, 
        with every time a new instruction is requested with the goal as well.
        """

        # get the environment minimum and maximum boundaries to shift from pixels into meters
        lower_bound, upper_bound = self.sim.pathfinder.get_bounds()
        min_x, max_x = lower_bound[0], upper_bound[0]
        min_y, max_y = lower_bound[2], upper_bound[2]

        top_down_map = self.get_topdown_view()
        # for angle, cpoint in zip(self.angles_checkpoints, checkpoints[1:]):
        #     print(cpoint)
        #     print(angle)
        #     maps.draw_agent(top_down_map, cpoint, int(angle), agent_radius_px=14)

        plt.figure(figsize=(12, 8))
        ax = plt.subplot(1, 1, 1)
        ax.axis("off")
        im = ax.imshow(top_down_map)

        # only plot the position if there is a significant change in the agent's location
        if len(self.agent_position) > 2:
            agent_position = np.array(self.agent_position)
            x = (agent_position[:, 0] + abs(min_x)) / self.meters_per_pixel
            y = (agent_position[:, 2] + abs(min_y)) / self.meters_per_pixel
            ax.scatter(x, y, s=3)

        # add the goal
        if goal is not None:
            ax.scatter((goal[0] + abs(min_x)) / self.meters_per_pixel,
                       (goal[2] + abs(min_y)) / self.meters_per_pixel, c='r', s=100, marker="X")

        # add the points where the user requested new instructions    
        if checkpoints is not None:
            agent_checkpoints = np.array(checkpoints)
            size = len(checkpoints)
            x_1 = (agent_checkpoints[:, 0] + abs(min_x)) / self.meters_per_pixel
            y_1 = (agent_checkpoints[:, 2] + abs(min_y)) / self.meters_per_pixel
            # print(angles)
            for i in range(size):
                t = mpl.markers.MarkerStyle(marker='^')
                t._transform = t.get_transform().rotate_deg(int(angles[i]))
                plt.scatter(x_1[i], y_1[i], c='b', marker=t, s=300)

            # ax.scatter(x_1, y_1, c='b', s=100, marker="p")

        plt.savefig(f"topdown_map/{self.env_name}_{self.random_seeds[self.seed_index]}_agent_map.png")

    def navmesh_visualization(self) -> None:

        semantic_list = []
        topdown_map_list = []

        if self.start is not None:
            sample1 = self.start
        else:
            sample1 = self.start_point

        if self.goal is not None:
            sample2 = self.goal[-1]
        else:
            sample2 = self.sim.pathfinder.get_random_navigable_point()

        path = habitat_sim.ShortestPath()
        path.requested_start = sample1
        path.requested_end = sample2
        found_path = self.sim.pathfinder.find_path(path)
        geodesic_distance = path.geodesic_distance
        path_points = path.points
        points_count = len(path_points)

        display = False
        map_path = None
        agent = self.sim.agents[self.agent_id]

        if found_path:
            if display:
                top_down_map = self.get_topdown_view()
                grid_dimensions = (top_down_map.shape[0], top_down_map.shape[1])
                for i in range(2, points_count + 1):
                    trajectory = [
                        maps.to_grid(
                            path_point[2],
                            path_point[0],
                            grid_dimensions,
                            pathfinder=self.sim.pathfinder,
                        )
                        for path_point in path_points[i - 2:i]
                    ]
                    grid_tangent = mn.Vector2(
                        trajectory[1][1] - trajectory[0][1], trajectory[1][0] - trajectory[0][0]
                    )
                    path_initial_tangent = grid_tangent / grid_tangent.length()
                    initial_angle = math.atan2(path_initial_tangent[0], path_initial_tangent[1])
                    maps.draw_path(top_down_map, trajectory)
                    maps.draw_agent(
                        top_down_map, trajectory[0], initial_angle, agent_radius_px=14
                    )

                    topdown_map_list.append(top_down_map)
                    if i == points_count:
                        maps.draw_agent(
                            top_down_map, trajectory[-1], initial_angle, agent_radius_px=14
                        )
                        self.display_save_map(top_down_map,
                                              f"topdown_map/{self.env_name}_{self.random_seeds[self.seed_index]}_generated_map.png")

        return path_points, semantic_list, topdown_map_list

    def print_scene_recur(self, scene, limit_output=1000) -> None:
        """
        Display some sample objects names, ids, sizes and locations within the environment
        """
        print(
            f"House has {len(scene.levels)} levels, {len(scene.regions)} regions and {len(scene.objects)} objects"
        )
        print(f"House center:{scene.aabb.center} dims:{scene.aabb.sizes}")
        # print(scene.objects)
        count = 0
        if len(scene.regions) > 0:
            for level in scene.levels:
                print(
                    f"Level id:{level.id}, center:{level.aabb.center},"
                    f" dims:{level.aabb.sizes}"
                )
                for region in level.regions:
                    print(
                        f"Region id:{region.id}, category:{region.category.name()},"
                        f" center:{region.aabb.center}, dims:{region.aabb.sizes}"
                    )
                    for obj in region.objects:
                        print(
                            f"Object id:{obj.id}, category:{obj.category.name()},"
                            f" center:{obj.aabb.center}, dims:{obj.aabb.sizes}"
                        )
                        count += 1
                        if count >= limit_output:
                            return


        elif len(scene.objects) > 0:
            for obj in scene.objects:
                if obj is not None:
                    print(
                        f"Object id:{obj.id}"
                    )
                    if obj.category is not None:
                        print(
                            f"Object id:{obj.id}, category:{obj.category.name()},"
                            f" center:{obj.aabb.center}, dims:{obj.aabb.sizes}"
                        )
                count += 1
                if count >= limit_output:
                    return

    # deprecated         
    def find_closest_object_in_view(self, scene, point, limit_output=1000):
        """
        Find the closes object to a specific point. Here we are ignoring the y dimension.
        The cosine distance is being used as a default; however other distances might be preferred.
        Some categories are being excluded, such as the wall and floor.
        """

        count = 0
        min_dist = 9999
        object_name = ""
        object_region = ""
        object_id = 0

        for level in scene.levels:
            for region in level.regions:
                for obj in region.objects:
                    object_distance = distance.cosine(point[[0, 2]], obj.aabb.center[[0, 2]])
                    if min_dist > object_distance and obj.category.name() not in self.BAD_CATEGORIES:
                        min_dist = object_distance
                        object_name = obj.category.name()
                        object_region = region.category.name()
                        object_id = obj.id
                        print(object_id)
                    count += 1
                    if count >= limit_output:
                        break

        return min_dist, object_name, object_region, object_id

    # deprecated
    def find_closest_object_in_view_semantic(self, scene, semantic, point, limit_output=1000):
        """
        Same as the previous function with the added value of integrating the semantic information in the 
        current camera's point of view in an (and) manner.
        """
        count = 0
        min_dist = 9999
        object_name = ""
        object_region = ""
        object_id = 0

        for level in scene.levels:
            for region in level.regions:
                for obj in region.objects:
                    object_distance = distance.cosine(point[[0, 2]], obj.aabb.center[[0, 2]])
                    if int(obj.id.split("_")[-1]) in np.unique(semantic):
                        if min_dist > object_distance and obj.category.name() not in self.BAD_CATEGORIES:
                            min_dist = object_distance
                            object_name = obj.category.name()
                            object_region = region.category.name()
                            object_id = obj.id
                    count += 1
                    if count >= limit_output:
                        break

        return min_dist, object_name, object_region, object_id

    # deprecated
    def rule_based_instruction_converter(self, path_points, semantic_list) -> list:
        steps = []
        objects = []
        regions = []
        scene = self.sim.semantic_scene
        if semantic_list is not None:
            _, current_nearest_object, current_region, current_object_id = \
                self.find_closest_object_in_view_semantic(scene, semantic_list[0], path_points[0])
        else:
            _, current_nearest_object, current_region, current_object_id = \
                self.find_closest_object_in_view_semantic(scene, path_points[0])

        indecies = []
        for index in range(0, len(path_points) - 1):
            if semantic_list is not None:
                min_dist, object_name, object_region, object_id = \
                    self.find_closest_object_in_view_semantic(scene, semantic_list[index], path_points[index])
            else:
                min_dist, object_name, object_region, object_id = \
                    self.find_closest_object_in_view(scene, path_points[index])
            print(min_dist, object_name, object_region)
            object_name = " ".join(object_name.split("_"))
            objects.append(object_name)
            regions.append(object_region)
            step = None
            region = ""
            if objects[index] == "door":
                region = f" of the {object_region}"
            if index == 0:
                if region == "":
                    region = f" located in the {object_region}"
                step = f"Move towards the {object_name}{region}"

            if index > 0:
                if regions[index - 1] != regions[index]:
                    region = f" located in the {object_region}"

                if objects[index - 1] != objects[index]:
                    if regions[index - 1] == regions[index]:
                        step = f"then move towards the {object_name}{region}"
                    else:
                        step = f"Move towards the {object_name}{region}"
                elif objects[index - 1] == objects[index] and regions[index - 1] != regions[index]:
                    region = f" located in the {object_region}"
                    step = f"Move towrds the {object_name}{region}"

            if step is not None:
                indecies.append(index)
                steps.append(step)
        return steps

    # TODO: NLP merge

    # Part 1: Looking around at a point
    def display_current_obs(self) -> None:

        "Get and display current observations"

        observations = self.sim.get_sensor_observations()

        rgb = observations["color_sensor"]
        semantic = observations["semantic_sensor"]
        depth = observations["depth_sensor"]

        plt.figure(figsize=(12, 8))
        ax = plt.subplot(1, 1, 1)
        ax.axis("off")

        rgb_img = Image.fromarray(rgb, mode="RGBA")
        plt.imshow(rgb_img)
        plt.show()

        self.display_sample(rgb, semantic, depth)

    def display_position(self, position=None, angle=0.) -> None:

        """
        Display the map with agent position.
        """

        top_down_map = self.get_topdown_view()

        grid_dimensions = (top_down_map.shape[0], top_down_map.shape[1])

        pos_to_display = (self.sim.agents[self.agent_id].get_state().position if position is None else position)
        print("Position:", pos_to_display)

        maps.draw_agent(top_down_map,
                        maps.to_grid(pos_to_display[2], pos_to_display[0], grid_dimensions, pathfinder=sim.pathfinder),
                        -angle, 10)
        self.display_save_map(top_down_map, "position_map.png")

    def set_agent_angle(self, angle):

        "Set the pose angle of the agent"

        cur_state = self.sim.agents[self.agent_id].get_state()
        cur_state.rotation = utils.quat_from_angle_axis(angle, np.array([0., 1., 0.]))
        self.sim.agents[self.agent_id].set_state(cur_state)

    def set_agent_position(self, pos):
        cur_state = self.sim.agents[self.agent_id].get_state()
        cur_state.position = pos
        self.sim.agents[self.agent_id].set_state(cur_state)

    def look_around(self, position=None):  # Not required

        "Look around in a full circle and display all observations with map"

        cur_state = agent.get_state()
        self.set_agent_angle(math.pi)

        if not position is None:
            self.set_agent_position(position)

        for i in range(13):
            self.sim.agents[self.agent_id].act("turn_right")
            self.display_position(angle=i * math.pi / 6)
            self.display_current_obs()
        self.sim.agents[self.agent_id].set_state(cur_state)

    # Part 2: Closest objects in the neighborhood
    def object_generator(self, scene):
        """
        Yield all objects from a scene.
        Yields (<object>, <category_name>) pairs.
        """
        for level in scene.levels:
            for region in level.regions:
                for obj in region.objects:
                    if obj.category.name() not in self.BAD_CATEGORIES:
                        yield obj, region.category.name()

    def find_closest_objects(self, scene, point, count,
                             distance_fun=distance.euclidean):
        """
        Return info on the count many object closest to point.
        Returns a list of (<distance>, <region_name>, <object>) tuples.
        """
        closest = heapq.nsmallest(count, self.object_generator(scene),
                                  lambda x: distance_fun(point[[0, 1, 2]], x[0].aabb.center[[0, 1, 2]]))
        return [(distance_fun(point, x[0].aabb.center), x[1], x[0]) for x in closest]

    def print_closest_objects_to_agent(self):
        for dist, region_name, obj in self.find_closest_objects(scene,
                                                                self.sim.agents[self.agent_id].get_state().position, 8):
            print(dist, region_name, obj.category.name(), obj.id)

    def check_points_distance(self, points, threshold=2.0):
        """
        Select points based on their distance from the next step
        if it's less than a threshold then the second point is removed
        """
        points = points.tolist()
        if len(points) > 2:

            i = len(points) - 1
            start = points[0]

            while i > 0:
                if distance.euclidean(points[i], points[i - 1]) < threshold:
                    points.pop(i - 1)
                i -= 1

            if distance.euclidean(points[0], start) < threshold:
                points[0] = start

        return np.array(points)

    # Part 3: Generating the first step in a random path
    def generate_random_goal(self):

        "Generate a random path and return the first two points"

        path = habitat_sim.ShortestPath()
        found_path = False
        while not found_path:
            start = self.sim.agents[self.agent_id].get_state().position
            goal = self.sim.pathfinder.get_random_navigable_point()
            path.requested_start = start
            path.requested_end = goal
            found_path = self.sim.pathfinder.find_path(path)

        return path.points[0], path.points[1:]

    def generate_random_step(self, goal=None):

        "Generate a random path and return the first two points"

        path = habitat_sim.ShortestPath()
        found_path = False

        while not found_path:
            start = self.sim.agents[self.agent_id].get_state().position
            path.requested_start = start
            path.requested_end = goal[-1]
            found_path = self.sim.pathfinder.find_path(path)

        return path.points[0], path.points[-2:]

    # Part 4: "Semantic map"
    def obj_region(self, obj):
        obj_region_id = "_".join(obj.id.split("_")[:2])
        for region in self.scene.regions:
            if region.id == obj_region_id:
                return region

    def obj_info(self, obj):
        return f"id: {obj.id}\nobj category: '{obj.category.name()}'\nobj region category: '{obj_region(obj).category.name()}'\nobj center: {obj.aabb.center}\nobj sizes: {obj.aabb.sizes}"

    def obj_corners(self, obj):
        """
        Return the 4 corners of an object's bounding box.
        """
        x_center, y_center = obj.aabb.center[0], obj.aabb.center[2]
        x_size, y_size = obj.aabb.sizes[0], obj.aabb.sizes[2]
        return [(x_center + x_size / 2, y_center + y_size / 2),
                (x_center - x_size / 2, y_center + y_size / 2),
                (x_center + x_size / 2, y_center - y_size / 2),
                (x_center - x_size / 2, y_center - y_size / 2)]

    def obj_vertical_corners(self, obj):
        x_center, y_center = obj.aabb.center[1], obj.aabb.center[2]
        x_size, y_size = obj.aabb.sizes[1], obj.aabb.sizes[2]
        return [(x_center + x_size / 2, y_center + y_size / 2),
                (x_center - x_size / 2, y_center + y_size / 2),
                (x_center + x_size / 2, y_center - y_size / 2),
                (x_center - x_size / 2, y_center - y_size / 2)]

    def display_objs(self, objs, key_points=None):

        top_down_map = self.get_topdown_view()
        grid_dimensions = (top_down_map.shape[0], top_down_map.shape[1])

        plt.figure(figsize=(12, 8))
        ax = plt.subplot(1, 1, 1)
        ax.axis("off")
        # ax.set_xlim(-11, 4.5)
        plt.imshow(top_down_map)

        for obj in objs:
            displayed_corner_coords = [maps.to_grid(corner_y, corner_x, grid_dimensions, pathfinder=sim.pathfinder) \
                                       for corner_x, corner_y in self.obj_corners(obj)]
            anchor = displayed_corner_coords[3][1], displayed_corner_coords[3][0]
            height = displayed_corner_coords[0][0] - displayed_corner_coords[2][0]
            width = displayed_corner_coords[0][1] - displayed_corner_coords[1][1]
            rect = patches.Rectangle(anchor, width, height, edgecolor="black", alpha=0.3, facecolor="lightblue")

            ax.add_patch(rect)
            ax.text(displayed_corner_coords[0][1], displayed_corner_coords[0][0],
                    obj.category.name(), fontsize=12, color="red", alpha=0.8)

        if key_points is not None:
            for point in key_points:
                y, x = maps.to_grid(point[2], point[0], grid_dimensions, pathfinder=sim.pathfinder)
                plt.plot(x, y, marker="o", markersize=10, alpha=0.8)

        plt.show(block=False)

    # Part 5: Characterising landmark-target relationships
    def is_in_bb(self, point, bb):
        """
        Return whether point is in bounding box bb.
        """
        x = point[0]
        y = point[2]
        x_center, y_center = bb.center[0], bb.center[2]
        x_size, y_size = bb.sizes[0], bb.sizes[2]
        x_is_in = x <= x_center + x_size / 2 and x >= x_center - x_size / 2
        y_is_in = y <= y_center + y_size / 2 and y >= y_center - y_size / 2
        return x_is_in and y_is_in

    def is_covered_by(self, point, obj):
        """
        Return whether point is actually in obj's bounding box.
        """
        return self.is_in_bb(point, obj.aabb)

    def is_near_to(self, point, obj, near_dist=None):

        """
        Return whether point is near to object.
        """

        if near_dist is None:
            near_dist = self.NEAR_DIST

        x = point[0]
        y = point[2]

        x_center, y_center = obj.aabb.center[0], obj.aabb.center[2]
        x_size, y_size = obj.aabb.sizes[0], obj.aabb.sizes[2]

        x_is_in = x > x_center - x_size / 2 - near_dist and x < x_center + x_size / 2 + near_dist
        y_is_in = y > y_center - y_size / 2 - near_dist and y < y_center + y_size / 2 + near_dist

        return x_is_in and y_is_in and not self.is_covered_by(point, obj)

    def rotate_clockwise_90(self, v):
        """
        Rotate planar sim vector v clockwise by 90 degrees.
        """
        rot = np.array([[0, -1], [1, 0]])
        return np.dot(rot, v)

    def rotate_clockwise_90(self, v):
        """
        Rotate planar sim vector v clockwise by 90 degrees.
        """
        rot = np.array([[0, -1], [1, 0]])
        return np.dot(rot, v)

    def v_length(self, v):
        """
        Return the length of vector v.
        """
        return math.sqrt(np.dot(v, v))

    def v_angle(self, v1, v2):
        """
        Return the angle between vectors v1 and v2.
        """
        return np.arccos(np.dot(v1, v2) / (self.v_length(v1) * self.v_length(v2)))

    def project_to(self, v, a):
        """
        Project vector v to the axis represented by vector a.
        """
        # print("v:", v, "a:", a, "projection: ", np.dot(v, a) / self.v_length(a))
        return np.dot(v, a) / self.v_length(a)

    def dist_from_line(self, p, v, o):
        """
        Return the signed distance of point p from line determined by point o and 
        direction vector v.
        """
        v_rot = self.rotate_clockwise_90(v)
        return self.project_to(p - o, v_rot)

    def vertical_orientation(self, point, landmark, agent_pos):

        """
        Return the vertical orientation of point relative to a landmark 
        from the agent's view point.
        """

        l_center = landmark.aabb.center[[1, 2]]
        point_2d = point[[1, 2]]
        agent_2d = agent_pos[[1, 2]]
        l_corners = self.obj_vertical_corners(landmark)

        agent_to_center = l_center - agent_2d
        agent_to_center_rot = self.rotate_clockwise_90(agent_to_center)

        extreme_top_corner = max(l_corners, key=lambda v: self.project_to(v - agent_2d, agent_to_center_rot))
        extreme_bottom_corner = min(l_corners, key=lambda v: self.project_to(v - agent_2d, agent_to_center_rot))

        top_dist = self.dist_from_line(point_2d, agent_to_center, extreme_top_corner)
        bottom_dist = self.dist_from_line(point_2d, agent_to_center, extreme_bottom_corner)

        if top_dist > 0:
            return "above"
        elif bottom_dist < 0:
            return "below"
        else:
            return "central"

    def lateral_orientation(self, point, landmark, agent_pos):

        """
        Return the lateral orientation of point relative to a landmark 
        from the point of view an agent.
        """

        l_center = landmark.aabb.center[[0, 2]]
        l_corners = self.obj_corners(landmark)

        point_2d = point[[0, 2]]
        agent_2d = agent_pos[[0, 2]]

        agent_to_center = l_center - agent_2d
        agent_to_center_rot = self.rotate_clockwise_90(agent_to_center)

        extreme_right_corner = max(l_corners, key=lambda v: self.project_to(v - agent_2d, agent_to_center_rot))
        extreme_left_corner = min(l_corners, key=lambda v: self.project_to(v - agent_2d, agent_to_center_rot))

        right_dist = self.dist_from_line(point_2d, agent_to_center, extreme_right_corner)
        left_dist = self.dist_from_line(point_2d, agent_to_center, extreme_left_corner)

        if right_dist > 0:
            return "right"
        elif left_dist < 0:
            return "left"
        else:
            return "central"

    def depth_orientation(self, point, landmark, agent_pos):

        """
        Return the depth orientation of point relative to a landmark 
        from the point of view an agent.
        """

        l_center = landmark.aabb.center[[0, 2]]
        l_corners = self.obj_corners(landmark)

        point_2d = point[[0, 2]]
        agent_2d = agent_pos[[0, 2]]

        agent_to_center = l_center - agent_2d
        agent_to_center_rot = self.rotate_clockwise_90(agent_to_center)

        closest_corner = min(l_corners, key=lambda v: self.project_to(v - agent_2d, agent_to_center))
        furthest_corner = max(l_corners, key=lambda v: self.project_to(v - agent_2d, agent_to_center))

        closest_dist = self.dist_from_line(point_2d, agent_to_center_rot, closest_corner)
        furthest_dist = self.dist_from_line(point_2d, agent_to_center_rot, furthest_corner)

        if closest_dist > 0:
            return "before"
        elif furthest_dist < 0:
            return "behind"
        else:
            return "in_line"

    def point_region(self, point):
        """
        Return the region of a point.
        """
        for region in self.scene.regions:
            if self.is_in_bb(point, region.aabb):
                return region

    def visible_obj_ids(self, view_point, angle):

        """
        Return a vector containing the numeric object ids visible from
        view_point in angle.
        """

        cur_state = self.sim.agents[self.agent_id].get_state()
        cur_position = cur_state.position
        cur_rotation = cur_state.rotation
        cur_state.position = view_point
        cur_state.rotation = utils.quat_from_angle_axis(angle, np.array([0., 1., 0.]))
        self.sim.agents[self.agent_id].set_state(cur_state)
        sem_obs = self.sim.get_sensor_observations()["semantic_sensor"]
        cur_state.position = cur_position
        cur_state.rotation = cur_rotation
        self.sim.agents[self.agent_id].set_state(cur_state)
        return (np.unique(sem_obs, return_counts=True)), sem_obs

    def angle_toward(self, view_point, position):
        """
        Return the angle from viewpoint towards position.
        """
        view_point_2d = view_point[[0, 2]]
        position_2d = position[[0, 2]]
        vp_to_pos = position_2d - view_point_2d
        return math.atan2(vp_to_pos[0], vp_to_pos[1]) + math.pi

    def pixel_angle(self, position_2d, center_pix=np.array([1600 // 2, 900 // 2])):
        """
        Return the angle from the image center pixel towards a given pixel
        """
        return math.degrees(math.atan2(position_2d[0] - center_pix[0], position_2d[1] - center_pix[1]))

    def visible_objects_looking_toward(self, view_point, position):
        """
        Return a list of objects visible from view_point looking towars position.
        The list is sorted by closeness to position.
        """

        angle = self.angle_toward(view_point, position)
        (visible_objects, visible_objects_pix_count), sem_obs = self.visible_obj_ids(view_point, angle)

        if len(visible_objects) == 0:
            return None

        objs_in_view = [self.obj_id_to_obj[id] for id in visible_objects if id in self.obj_id_to_obj]

        objs = []
        distance_list = []
        visible_objects = []
        for i, obj in enumerate(objs_in_view):
            if obj.category is not None:
                if obj.category.name() not in self.BAD_CATEGORIES:
                    objs.append(obj)
                    distance_list.append(distance.euclidean(position[[0, 2]], obj.aabb.center[[0, 2]]))
                    visible_objects.append(visible_objects_pix_count[i])

        sorted_indices = np.argsort(distance_list)
        return np.array(objs)[sorted_indices], np.array(visible_objects)[sorted_indices], sem_obs

    def region_info(self, region):
        return f"Region id: {region.id}, region category: {region.category.name()}"

    def pos_info(self, position, obj, view_point):
        print("== Spatial configuration info ==")
        print("View point:", region_info(self.point_region(view_point)))
        print("Position:", region_info(self.point_region(position)))
        print("lateral orientation:", lateral_orientation(position, obj, view_point))
        print("depth orientation:", depth_orientation(position, obj, view_point))
        print("position is near to obj:", is_near_to(position, obj))
        print("position is covered by the obj:", is_covered_by(position, obj))

    # Mapping spatial configurations to instructions

    def turn_instruction(self, angle):
        """
        Return an instruction for turning (horizontally) by angle. Angle should be in radian.
        """
        a = math.degrees(angle) % 360  # we convert to degrees in [0, 360) for easier rules
        change_threshold = 45  # degrees
        min_degree = 0
        max_degree = 360
        logger.info(f"Angle: {a}")

        if min_degree + change_threshold >= a or a >= max_degree - change_threshold:
            return None

        elif 90 - change_threshold < a <= 90 + change_threshold:
            return random.choice(["Turn left",
                                  "Turn to the left",
                                  "Make a left turn",
                                  "Look to the left side"])

        elif 180 - change_threshold < a <= 180 + change_threshold:
            return random.choice(["Turn around",
                                  "Turn to the opposite direction"])

        elif 270 - change_threshold < a < 270 + change_threshold:
            return random.choice(["Turn right",
                                  "Turn to the right",
                                  "Make a right turn",
                                  "Look to the right side"])
        else:
            logger.warning("Angle error: this degree is not accounted for!!")
            return None

    def clock_turn_instruction(self, angle):
        """
        Return an instruction for turning (horizontally) by angle. Angle should be in radian.
        """
        angle = math.degrees(angle) % 360  # we convert to degrees in [0, 360) for easier rules
        change_threshold = 15  # degrees
        min_degree = 0
        max_degree = 360
        logger.info(f"Angle: {angle}")

        hand = 12
        if min_degree + change_threshold >= angle or angle >= max_degree - change_threshold:
            return None
        elif 15 < angle <= 45:
            hand = 1
        elif 45 < angle <= 75:
            hand = 2
        elif 75 < angle <= 105:
            hand = 3
        elif 105 < angle <= 135:
            hand = 4
        elif 135 < angle <= 165:
            hand = 5
        elif 165 < angle <= 195:
            hand = 6
        elif 195 < angle <= 225:
            hand = 7
        elif 225 < angle <= 255:
            hand = 8
        elif 255 < angle <= 285:
            hand = 9
        elif 285 < angle <= 315:
            hand = 10
        elif 315 < angle <= 345:
            hand = 11

        return f"Turn towards {hand} o'clock"

    def look_at_angle_instructions(self, coordinates):

        """
        Return the corresponding angle in the form of clock tiks (12) giving image coordinates (x, y) 
        """
        angle = self.pixel_angle(coordinates)

        if -15 < angle <= 15:
            return 12
        elif 15 < angle <= 45:
            return 1
        elif 45 < angle <= 75:
            return 2
        elif 75 < angle <= 105:
            return 3
        elif 105 < angle <= 135:
            return 4
        elif 135 < angle <= 165:
            return 5
        elif 165 < angle <= 180 or -180 <= angle <= -165:
            return 6
        elif -165 < angle <= -135:
            return 7
        elif -135 < angle <= -105:
            return 8
        elif -105 < angle <= -75:
            return 9
        elif -75 < angle <= -45:
            return 10
        elif -45 < angle <= -15:
            return 11

    def remove_repeated_objs(self, objs):
        """
        Remove objects that are not unique in their category.
        objs is an iterable containing semantic objects.
        Returns a list.
        """
        counts = Counter(obj.category.name() for obj in objs)
        return [obj for obj in objs
                if obj.category.name() in self.important_objects
                if counts[obj.category.name()] == 1]

    def normalize_semantic_info(self, data):
        return (data - np.min(data)) / (np.max(data) - np.min(data)) * 1000

    def landmark_sort_key(self, obj, obj_pix, start, goal):
        """
        Numeric landmark sort keys for a given landmark and start and goal positions.
        """

        lateral_o = self.lateral_orientation(goal, obj, start)
        depth_o = self.depth_orientation(goal, obj, start)
        is_near = self.is_near_to(goal, obj)
        is_covered = self.is_covered_by(goal, obj)
        obj_category = obj.category.name()

        # best cases are near and not behind
        if (is_near or is_covered) and depth_o != "behind":
            if lateral_o == "central":
                key = 3000
            else:
                key = 2000

        # second best is at least in the right direction
        elif lateral_o == "central" and depth_o != "behind":
            key = 1000

        # else (for now) we look at the distance from the target 
        # (the farther the distance the less score the target gets):
        else:
            key = 500 - distance.euclidean(obj.aabb.center[[0, 2]], goal[[0, 2]])

        # we added the pixel count to the key so if there are two objects with the same key value the 
        # normalized count can boost the object that appears to be larger since the normalization is between 0 and 1000
        # this will only act as a deciding step between two equivalent keys from the previous conditions

        # penalize the distance and add the semantic pix
        key += obj_pix - (distance.euclidean(obj.aabb.center[[0, 2]], goal[[0, 2]]) * 10)

        # second key is the object category score, 
        # it can severely impact the results so for un categorized object it's giving an average of 4
        # key = key * (self.obj_category_landmark_score.get(obj_category, 4) / 10)

        return key

    def select_important_objects(self, obj, obj_pix, start, goal):
        if obj_pix < 50 or math.isnan(obj_pix): # if the object takes less than 5% of the image then ignore it
            print(f"{obj_pix} {obj.category.name()}")
            return 0
        if obj.category.name() == self.previous_pick:
            return 0
        if self.previous_pick == obj.category.name():
            return 0

        obj_id = int(obj.id.split("_")[-1])
        sorted_volume = dict(sorted(self.obj_id_to_volume.items(), key=operator.itemgetter(1), reverse=False))
        if obj_id in sorted_volume:
            res = list(sorted_volume.keys()).index(obj_id)
        else:
            res = 0

        if res < ((sum(self.obj_id_to_volume.values()) / len(self.obj_id_to_volume)) / 2):
            return 0

        key = obj_pix  # - (distance.euclidean(obj.aabb.center[[0, 2]], goal[[0, 2]]) * 10)
        key += self.obj_category_landmark_score.get(obj.category.name(), 4) * 30

        print(obj.category.name())
        print(key)
        return key

    def referring_exp(self, obj, obj_category=None):
        """
        Return an appropriate expression referring to obj.
        """

        if obj_category is None:
            obj_category = obj.category.name()
        else:
            obj_category = obj_category

        return obj_category

    def dist_exp(self, d):
        """
        Return an expression describing the distance d.
        """
        step_count = max(round(d / self.STEP_LENGTH), 1)
        if step_count == 1:
            return "one step"
        else:
            return f"{max(round(d / self.STEP_LENGTH), 1)} steps"

    def describe_repeated_objects(self, obj, objs, start):

        epsilon = 1.0
        size_constraint = 0
        lateral_o, depth_o = None, None

        match_counter = 0

        large = False
        small = False

        close = False
        far = False

        right = False
        left = False

        for tmp_obj in objs:
            if obj.category.name() == tmp_obj.category.name():
                match_counter += 1

                size_difference = np.prod(obj.aabb.sizes) - np.prod(tmp_obj.aabb.sizes)
                lateral_o = self.lateral_orientation(obj.aabb.center, tmp_obj, start)
                depth_o = self.depth_orientation(obj.aabb.center, tmp_obj, start)

                if size_difference - epsilon > 0:
                    large = True
                else:
                    large = False

                if size_difference + epsilon < 0:
                    small = True
                else:
                    small = False

                if lateral_o == "right":
                    right = True
                else:
                    right = False

                if lateral_o == "left":
                    left = True
                else:
                    left = False

                if depth_o == "before":
                    close = True
                else:
                    close = False

                if depth_o == "after":
                    far = True
                else:
                    far = False

        return match_counter > 1, (large, small), (right, left), (close, far)

    def clock_hand_turning_instructions_from_visible_objects(self, obj, sem_obs):
        """
        Giving a semantic image, retrive the unique values/objects centers, 
        and check what angle they are located ay
        """
        value = int(obj.id.split("_")[-1])

        x, y = np.where(sem_obs == value)

        x_center = x[len(x) // 2]
        y_center = y[len(y) // 2]

        return self.look_at_angle_instructions(coordinates=np.array([x_center, y_center]))

    def relabel_special_objs(self, obj, start, goal):

        category_name = obj.category.name()
        if category_name == "table":

            counter = 0
            for _, close_obj in self.obj_id_to_obj.items():
                dist = distance.euclidean(obj.aabb.center, close_obj.aabb.center)

                if dist < 2.:
                    if close_obj.category.name() == "chair":
                        counter += 1

            if counter > 1:
                obj_id = int(obj.id.split("_")[-1])
                if obj_id in self.obj_id_to_region.keys():
                    if self.obj_id_to_region[obj_id] == "kitchen":
                        return False, True, "dining table"

                return False, True, random.choice(["table with chairs", "table surrounded by chairs"])

        elif category_name == "stairs" or category_name =="stair":
            if start[1] == goal[1]:
                return False, False, category_name
            elif start[1] < goal[1]:
                sentence = random.choice(["go up stairs to the second floor, using the stairs"])
            else: #start[1] > goal[1]:
                sentence = random.choice(["go down stairs to the first floor, using the stairs"])

            return True, True, sentence

        return False, False, category_name

    def move_instruction(self, start, goal, turn_instruction=True):
        """
        Return an instruction for moving from start to goal __assuming that the agent's orientation/pose
        is already approximately correct__.
        """
        objs, objs_pix_count, sem_obs = self.visible_objects_looking_toward(start, goal)
        try:
            objs_pix_count = self.normalize_semantic_info(objs_pix_count)
        except:  # if there is only one object in the current view like a wall!
            pass
        ref_exp_size, ref_exp_lateral, ref_exp_depth, ref_exp_clock = None, None, None, None
        instruction_1, instruction_2, instruction_3, instruction_4, instruction_5 = None, None, None, None, None
        # unique_objs = self.remove_repeated_objs(objs)

        scores = []
        for obj, obj_pix, in zip(objs, objs_pix_count):
            scores.append(self.select_important_objects(obj, obj_pix, start, goal))

        if len(scores) > 1:
            top_3 = np.argsort(-np.array(scores))[:3]
            obj = random.choice(objs[top_3])
            print(top_3)
        else:
            obj = None

        dist_from_start = distance.euclidean(start[[0, 2]], goal[[0, 2]])

        # Case 0: No objects detected (fallback instruction)
        # This case should never occur in this situation if it did then we can use this instruction
        if obj is None:
            sentence = random.choice(["go to the middle of the room", "move to the room's center",
                                      "walk to the center of the room", "change the location to the room's center"
                                         , ])
            instruction = f"{sentence}, then we'll continue"

            return instruction

        else:  # if obj is not None
            obj_category = obj.category.name()
            issue_instruction, relabel, new_label = self.relabel_special_objs(obj, start, goal)

            if issue_instruction:
                return new_label

            lateral_o = self.lateral_orientation(goal, obj, start)
            depth_o = self.depth_orientation(goal, obj, start)
            vertical_o = self.vertical_orientation(goal, obj, start)

            is_near = self.is_near_to(goal, obj)
            is_covered = self.is_covered_by(goal, obj)

            if relabel:
                ref_exp = self.referring_exp(obj, new_label)
            else:
                ref_exp = self.referring_exp(obj)

            obj_id_clock_instruction = None
            obj_id_clock_instruction = self.clock_hand_turning_instructions_from_visible_objects(obj, sem_obs)
            multiple, (large, small), (right, left), (close, far) = self.describe_repeated_objects(obj, objs, start)

            if (large or small) and obj_category not in self.bad_size_categories:
                # Case 1: biggest object
                # in case multiple objects of the same type are present and the selected obj cub's volume has the greatest value
                if large:
                    sentence = random.choice(["big", "bigger", "biggest", "large", "largest", ])
                    ref_exp_size = f"{sentence} {ref_exp}"

                # Case 2: smallest object
                # in case multiple objects of the same type are present and the selected obj cub's volume has the lowest value
                elif small:
                    sentence = random.choice(["small", "smaller", "smallest"])
                    ref_exp_size = f"{sentence} {ref_exp}"

            if (right or left) and (not turn_instruction):
                # Case 3: obj is on the left or right
                if right:
                    ref_exp_lateral = f"most right {ref_exp}"
                if left:
                    ref_exp_lateral = f"most left {ref_exp}"

            if close or far:
                # Case 4: closest object
                if close:
                    sentence = random.choice(
                        ["close", "closer", "closest", "closeby", "near", "nearest", "nearby", ])

                # Case 5: furthest object
                elif far:
                    sentence = random.choice(["far", "further", "furthest", "most distant"])

                additional_expression = random.choice(["you can find",
                                                          "in your view",
                                                          "withing your sight"])
                ref_exp_depth = f"{sentence} {ref_exp} {additional_expression}"

            # Case 6: use clock instructions
            # if obj_id_clock_instruction is not None:
            #     ref_exp_clock = f"{ref_exp} at {obj_id_clock_instruction} o'clock"

            # if turn_instruction is None:
            #     ref_exp = ref_exp_clock

            # else:
            ref_list = []
            if ref_exp_size is not None:
                ref_list.append(ref_exp_size)
            if ref_exp_lateral is not None:
                ref_list.append(ref_exp_lateral)
            if ref_exp_depth is not None:
                ref_list.append(ref_exp_depth)
            # if ref_exp_clock is not None:
            #     ref_list.append(ref_exp_clock)

            if len(ref_list) > 0:
                ref_exp = random.choice(ref_list)

            if is_near or is_covered:
                if depth_o != "behind":
                    # Case 7: Move to the front of an object
                    if lateral_o == "central":
                        sentence = random.choice(["go forward to the",
                                                  "move forward to the",
                                                  "walk forward to the"])
                        instruction_1 = f"{sentence} {ref_exp}"

                    # Case 8: Move to the side of an object left/right
                    else:
                        sentence = random.choice(["go to the",
                                                  "move to the",
                                                  "walk to the"])
                        instruction_1 = f"{sentence} {ref_exp}"

                    if vertical_o == "central":
                        # Case 9: Move to the center of an object
                        sentence = random.choice(["go forward to the",
                                                  "move forward to the",
                                                  "walk forward to the"])
                        instruction_2 = f"{sentence} {ref_exp}"

                    else:
                        # Case 10: Go under something or above it (rare case)
                        sentence = random.choice(["go to the",
                                                  "move to the",
                                                  "walk to the"])
                        instruction_2 = f"{sentence} {ref_exp}"

            elif lateral_o == "central":
                sentence = random.choice(["go forward to the",
                                          "move forward to the",
                                          "walk forward to the"])
                instruction_1 = f"{sentence} {ref_exp}"
            # Case 11: the goal is behind an object
            else:
                sentence = random.choice(["go to the"])
                if lateral_o != "central":
                    instruction_5 = f"{sentence} {ref_exp}" #on the {lateral_o} side"
                else:
                    instruction_5 = f"{sentence} {ref_exp}"

        instruction_list = []
        if instruction_1 is not None:
            instruction_list.append(instruction_1)
        if instruction_2 is not None:
            instruction_list.append(instruction_2)
        if instruction_3 is not None:
            instruction_list.append(instruction_3)
        if instruction_4 is not None:
            instruction_list.append(instruction_4)
        if instruction_5 is not None:
            instruction_list.append(instruction_5)

        if len(instruction_list) == 0:
            sentence = random.choice(["go to the middle of the room",
                                      "move to the room's center",
                                      "walk to the center of the room",
                                      "change the location to the room's center" ])
            instruction = f"{sentence}, then we'll continue"
            instruction_list.append(instruction)

        instruction = random.choice(instruction_list)
        # try:
        #     obj_id = int(obj.id.split("_")[-1])
        #     if obj_id in self.obj_id_to_region.keys():
        #         if self.obj_id_to_region[obj_id] is not None:
        #             instruction = f"{instruction}, located in the {self.obj_id_to_region[obj_id]}"
        # except:
        #     pass

        return instruction

    def instruction(self, start_pos, start_angle, goal):
        """
        Return a verbal instruction for walking to goal from position start_pos with start angle start_angle.
        start_pos and goal are standard 3d habitat positions, while start_angle is the pose's rotation around the 
        [0, 1, 0] axis.
        """

        turn_angle = self.angle_toward(start_pos, goal) - start_angle
        turn_inst = self.turn_instruction(turn_angle)
        clock_inst = self.clock_turn_instruction(turn_angle)
        move_inst = self.move_instruction(start_pos, goal, turn_inst)

        if turn_inst is None:
            return move_inst.capitalize()
        else:
            sentence = random.choice(["and", "then", "afterwards"])
            inst = random.choice([clock_inst])
            return f"{inst}\n{sentence} {move_inst}"

    def euler_from_quaternion(self, x, y, z, w):
        """
        Convert a quaternion into euler angles (roll, pitch, yaw)
        roll is rotation around x in radians (counterclockwise)
        pitch is rotation around y in radians (counterclockwise)
        yaw is rotation around z in radians (counterclockwise)
        """
        t0 = +2.0 * (w * x + y * z)
        t1 = +1.0 - 2.0 * (x * x + y * y)
        roll_x = math.atan2(t0, t1)

        t2 = +2.0 * (w * y - z * x)
        t2 = +1.0 if t2 > +1.0 else t2
        t2 = -1.0 if t2 < -1.0 else t2
        pitch_y = math.asin(t2)

        t3 = +2.0 * (w * z + x * y)
        t4 = +1.0 - 2.0 * (y * y + z * z)
        yaw_z = math.atan2(t3, t4)

        return roll_x, pitch_y, yaw_z

    def navmesh_config_and_recompute(self) -> None:
        """
        This method is setup to be overridden in for setting config accessibility
        in inherited classes.
        """

        self.navmesh_settings = habitat_sim.NavMeshSettings()
        self.navmesh_settings.set_defaults()
        self.navmesh_settings.agent_height = self.cfg.agents[self.agent_id].height
        self.navmesh_settings.agent_radius = self.cfg.agents[self.agent_id].radius

        self.sim.recompute_navmesh(
            self.sim.pathfinder,
            self.navmesh_settings,
            include_static_objects=False,
        )

    def exit_event(self, event: Application.ExitEvent):
        """
        Overrides exit_event to properly close the Simulator before exiting the
        application.
        """
        self.sim.close(destroy=True)
        event.accepted = True
        exit(0)

    def print_help_text(self) -> None:
        """
        Print the Key Command help text.
        """
        logger.info(
            """
            =====================================================
            Welcome to the Habitat-sim Python Viewer application!
            =====================================================
            Mouse Functions ('m' to toggle mode):
            ----------------
            In LOOK mode (default):
                LEFT:
                    Click and drag to rotate the agent and look up/down.
                WHEEL:
                    Modify orthographic camera zoom/perspective camera FOV (+SHIFT for fine grained control)
            
            In GRAB mode (with 'enable-physics'):
                LEFT:
                    Click and drag to pickup and move an object with a point-to-point constraint (e.g. ball joint).
                RIGHT:
                    Click and drag to pickup and move an object with a fixed frame constraint.
                WHEEL (with picked object):
                    default - Pull gripped object closer or push it away.
                    (+ALT) rotate object fixed constraint frame (yaw)
                    (+CTRL) rotate object fixed constraint frame (pitch)
                    (+ALT+CTRL) rotate object fixed constraint frame (roll)
                    (+SHIFT) amplify scroll magnitude
            
            
            Key Commands:
            -------------
                esc:        Exit the application.
                'h':        Display this help message.
                'm':        Cycle mouse interaction modes (disable)
                
                Navigation Commands (important):
                'c':        Start the experiment.
                'i':        Issue a new instruction from the current agent's position to the goal.
                'b':        Save the current topdown view.
                'p':        Set the next random seed and reinitialize the environment.
                
                
                Agent Controls:
                'wsad':     Move the agent's body forward/backward and left/right.
                'zx':       Move the agent's body up/down.
                arrow keys: Turn the agent's body left/right and camera look up/down.
                
                # Ignore for the purpose of the experiment 
                Utilities:
                'r':        Reset the simulator with the most recently loaded scene.
                'n':        Show/hide NavMesh wireframe.
                            (+SHIFT) Recompute NavMesh with default settings.
                ',':        Render a Bullet collision shape debug wireframe overlay (white=active, green=sleeping, blue=wants sleeping, red=can't sleep)
            
                Object Interactions:
                SPACE:      Toggle physics simulation on/off.
                '.':        Take a single simulation step if not simulating continuously.
                'v':        (physics) Invert gravity.
                't':        Load URDF from filepath
                            (+SHIFT) quick re-load the previously specified URDF
                            (+ALT) load the URDF with fixed base
            =====================================================
            """
        )
