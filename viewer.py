import ctypes
import sys
import os

flags = sys.getdlopenflags()

sys.setdlopenflags(flags | ctypes.RTLD_GLOBAL)

import tkinter as tk
import threading

from utils.settings import default_sim_settings, make_cfg
from utils.interactive_viewer import HabitatSimInteractiveViewer


def init_env(path, instruction_label, step_label, default_sim_settings=default_sim_settings):
    sim_settings: Dict[str, Any] = default_sim_settings
    sim_settings["scene"] = path
    sim_settings["scene_dataset_config_file"] = "None"
    HabitatSimInteractiveViewer(sim_settings, path.split("/")[-2], step_label, instruction_label).exec()


def start_env():
    value = lb.get(lb.curselection())
    x = threading.Thread(target=init_env, args=(value, instruction_label, step_label, default_sim_settings,))
    x.start()


if not os.path.exists("topdown_map"):
    os.mkdir("topdown_map")

window = tk.Tk()
window.title('Navigation Experiment')
window.geometry('1920x1080+0+0')
window.configure(bg='white')

instruction_label = tk.StringVar()
step_label = tk.StringVar()

sl = tk.Label(window, bg='red', fg='black', font=('Arial', 10),
              width=10, height=1, textvariable=step_label)
sl.place(x=0, y=0)
sl.pack()

il = tk.Label(window, bg='white', fg='black', font=('Arial', 10),
              width=150, height=2, textvariable=instruction_label)
il.pack()

b1 = tk.Button(window, text='Select Environment',
               width=17, height=2, command=start_env)
b1.pack()

lb = tk.Listbox(window)
lb.config(width=100)

lb.insert(1, "data/Replica-Dataset/room_0/mesh_preseg_semantic.ply")
lb.insert(2, "data/Replica-Dataset/apartment_0/mesh_semantic.ply")
lb.insert(3, "data/Replica-Dataset/apartment_1/mesh_preseg_semantic.ply")
lb.insert(4, "data/Replica-Dataset/frl_apartment_0/mesh_semantic.ply")
lb.insert(5, "data/mp3d_example/17DRP5sb8fy.glb")

lb.pack()

step_label.set("Steps: 0.0")
window.mainloop()




